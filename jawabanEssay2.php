<?php

function hitung($string_data)
{
    $operator = '';
    $angka1 = '';
    $angka2 = '';
    for ($i = 0; $i < strlen($string_data); $i++) {
        if ($string_data[$i] === "+" || $string_data[$i] === "-" || $string_data[$i] === "*" || $string_data[$i] === ":" || $string_data[$i] === "%") {
            $operator = $string_data[$i];
            $angka1 = substr($string_data, 0, $i);
            $angka2 = substr($string_data, $i + 1, strlen($string_data) - 1);
            break;
        }
    }

    settype($angka1, "integer");
    settype($angka2, "integer");
    
    if ($operator == "+") {
        return $angka1 + $angka2;
    } else if ($operator == "-") {
        return $angka1 - $angka2;
    } else if ($operator == "*") {
        return $angka1 * $angka2;
    } else if ($operator == ":") {
        return $angka1 / $angka2;
    } else if ($operator == "%") {
        return $angka1 % $angka2;
    }
}

echo hitung("102*3");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");
